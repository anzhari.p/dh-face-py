# Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# PDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-rekognition-developer-guide/blob/master/LICENSE-SAMPLECODE.)

import boto3 

def search_faces_by_image(collectionId, fileBytes):
    threshold = 70
    maxFaces = 5
    client = boto3.client('rekognition')
    response = client.search_faces_by_image(CollectionId=collectionId,
                                            Image={
                                                "Bytes": fileBytes
                                            },
                                            FaceMatchThreshold=threshold,
                                            MaxFaces=maxFaces)
    faceMatches = response['FaceMatches']

    return faceMatches
