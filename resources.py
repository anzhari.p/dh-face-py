from flask_restful import Resource, reqparse, abort
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, exc
import logging

import aws_rekognition_add_face
import aws_rekognition_search_face

import config

import uuid

import werkzeug
import os
import io
import sys

import boto3
from botocore.exceptions import ClientError

import base64

import requests

logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(name)s %(process)d %(message)s')

class IndexFace(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'faceImage', type=werkzeug.datastructures.FileStorage, location='files', help='This field cannot be blank', required=True)
        parser.add_argument(
            'bansosReceiverId', help='This field cannot be blank', required=True)
        data = parser.parse_args()

        # Check if bansos_receiver_id already exist
        Base = automap_base()
        engine = create_engine(config.DBSTRING_DH_FACE_PY)
        try:
            Base.prepare(engine, reflect=True)
        except exc.SQLAlchemyError as e:
            logging.error(e)
            abort(500, message="DB access error")
        M_Face = Base.classes.m_face
        session = Session(engine)
        record = session.query(M_Face).filter_by(
            bansos_receiver_id=data["bansosReceiverId"]).first()
        if record is not None:
            logging.info("Existing bansos_receiver_id found: " +
                         str(data["bansosReceiverId"]))
            return {
                "message": "Existing bansos_receiver_id found"
            }

        # Auto-generate stored s3 object name using uuid
        object_name = str(uuid.uuid4())

        # Upload to S3s
        # Save the file ID objects
        s3 = boto3.client('s3')
        object_name = str(uuid.uuid4())
        stream = data["faceImage"]
        try:
            s3.upload_fileobj(stream, config.AWS_BUCKET,
                              object_name, ExtraArgs={'ACL': 'public-read'})
        except ClientError as e:
            logging.error(e)
            abort(500, message="Object storage access error")
        # Index to Rekognition
        # Save the Face ID
        try:
            add_face = aws_rekognition_add_face.add_faces_to_collection(
                config.AWS_BUCKET, object_name, config.AWS_REKOGNITION_COLLECTION_ID)
        except ClientError as e:
            logging.error(e)
            abort(500, message="Facial recognition API access error")
        # Record to DB
        # Record the bansosReceiverID and AWS faceId to DB
        Base = automap_base()
        # engine, suppose it has two tables 'user' and 'address' set up
        engine = create_engine(config.DBSTRING_DH_FACE_PY)

        # reflect the tables
        try:
            Base.prepare(engine, reflect=True)
        except exc.SQLAlchemyError as e:
            logging.error(e)
            abort(500, message="DB access error")

        # mapped classes are now created with names by default
        # matching that of the table name.
        M_Face = Base.classes.m_face

        # session = Session(engine)
        session = Session(engine)

        # rudimentary relationships are produced
        try:
            session.add(M_Face(
                id=uuid.uuid4(), bansos_receiver_id=data["bansosReceiverId"], s3_url=config.AWS_BUCKET_URL + object_name, aws_face_id=add_face[1]))
            session.commit()
        except exc.SQLAlchemyError as e:
            logging.error(e)
            abort(500, message="DB access error")
        # Return data to client
        # Return s3 URL, faceId, bansosReceiverID
        result = {
            "faceId": add_face[1],
            "bansosReceiverId": data["bansosReceiverId"],
            "s3Url": config.AWS_BUCKET_URL + object_name
        }
        return result


class SearchFace(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'faceImage', type=werkzeug.datastructures.FileStorage, location='files', help='This field cannot be blank', required=True)
        data = parser.parse_args()

        result = []
        # logging.info(parser.content_length)
        # parser.content_length

        # Instantiate SQLAlchemy DB AutoMap dh-face-py
        # try:
        #     BaseDHFacePy = automap_base()
        #     engine_dh_face_py = create_engine(config.DBSTRING_DH_FACE_PY)
        #     BaseDHFacePy.prepare(engine_dh_face_py, reflect=True)
        #     M_Face = BaseDHFacePy.classes.m_face
        #     session_dh_face_py = Session(engine_dh_face_py)
        # except exc.SQLAlchemyError as e:
        #     print(e)

        # Instantiate SQLAlchemy DB AutoMap bansos_receiver
        # try:
        #     BaseBansosReceiver = automap_base()
        #     engine_bansos_receiver = create_engine(config.DBSTRING_BANSOS_RECEIVER)
        #     BaseBansosReceiver.prepare(engine_bansos_receiver, reflect=True)
        #     Bansos_Receiver = BaseBansosReceiver.classes.bansos_receiver
        #     session_bansos_receiver = Session(engine_bansos_receiver)
        # except exc.SQLAlchemyError as e:
        #     print(e)

        base64_image = base64.b64encode(data["faceImage"].read())

        base_64_binary = base64.decodebytes(base64_image)

        # AWS Rekognition compare with existing collection
        try:
            faceMatches = aws_rekognition_search_face.search_faces_by_image(
                config.AWS_REKOGNITION_COLLECTION_ID, base_64_binary)
        except ClientError as e:
            logging.error(e)
            abort(500, message="Facial recognition API access error")
        # print(faceMatches)
        if not faceMatches:
            logging.info("No similar face found")
            abort(404, message="No similar face found")

        # Get data from dh-face-py table

        for faceMatch in faceMatches:
            logging.info('FaceId: ' + faceMatch['Face']['FaceId'] +
                         ', Similarity: ' + "{:.2f}".format(faceMatch['Similarity']) + "%")
            Base = automap_base()
            engine = create_engine(config.DBSTRING_DH_FACE_PY)
            try:
                Base.prepare(engine, reflect=True)
            except exc.SQLAlchemyError as e:
                logging.error(e)
                abort(500, message="DB access error")
            M_Face = Base.classes.m_face
            session = Session(engine)

            # From AWS Rekognition
            faceId = faceMatch["Face"]["FaceId"]
            similarity = str(faceMatch["Similarity"])
            # From dh-bansos-py
            bansosReceiverId = ""
            s3url = ""
            # From bansos_receiver
            bansosReceiverNamaKrt = ""
            bansosReceiverNikKrt = ""
            bansosReceiverMsisdn = ""
            type = "BANSOS_RECEIVER"
            entityId = ""
            try:
                record = session.query(M_Face).filter_by(
                    aws_face_id=faceId).first()
            except exc.SQLAlchemyError as e:
                logging.error(e)
                abort(500, message="DB access error")
            # record = session_dh_face_py.query(M_Face).filter_by(
            #     aws_face_id=faceId).first()
            if record is not None:
                bansosReceiverId = record.bansos_receiver_id
                logging.info("bansosReceiverId: " + str(bansosReceiverId))
                # print("bansosReceiverId = record.bansos_receiver_id",
                #       bansosReceiverId)
                s3url = record.s3_url

                Base = automap_base()
                engine = create_engine(config.DBSTRING_BANSOS_RECEIVER)
                try:
                    Base.prepare(engine, reflect=True)
                except exc.SQLAlchemyError as e:
                    logging.error(e)
                    abort(500, message="DB access error")
                Bansos_Receiver = Base.classes.bansos_receiver
                session = Session(engine)
                try:
                    record = session.query(Bansos_Receiver).filter_by(
                        id=bansosReceiverId).first()
                except exc.SQLAlchemyError as e:
                    logging.error(e)
                    abort(500, message="DB access error")
                # record = session_bansos_receiver.query(Bansos_Receiver).filter_by(
                #     id=bansosReceiverId).first()
                if record is not None:
                    # print("Bansos_Receiver", record.id,
                    #       record.nik_krt, record.no_hp)
                    logging.info("Bansos_Receiver: ", str(record.id),
                                 str(record.nik_krt), str(record.no_hp))
                    bansosReceiverId = record.id
                    bansosReceiverNamaKrt = record.nama_krt
                    bansosReceiverNikKrt = record.nik_krt
                    bansosReceiverMsisdn = record.no_hp

                    # API call to get entity_id
                    if bansosReceiverMsisdn is not None:
                        # print ("bansosReceiverMsisdn", bansosReceiverMsisdn)
                        logging.info("Call bansos API for MSISDN: ",
                                     str(bansosReceiverMsisdn))
                        url = config.INTREGRATOR_BASE_URL + bansosReceiverMsisdn
                        payload = {}
                        headers = {}
                        try:
                            response = requests.request(
                                "GET", url, headers=headers, data=payload)
                        except requests.RequestException as e:
                            logging.error(e)
                            abort(500, message="Backend API access error")
                        logging.info("Retrieved bansos_receiver: ", bansosReceiverId,
                                     bansosReceiverNamaKrt, bansosReceiverMsisdn)
                        # print("bansosReceiverId", bansosReceiverId)
                        # print("bansosReceiverNamaKrt", bansosReceiverNamaKrt)
                        # print("bansosReceiverMsisdn", bansosReceiverMsisdn)
                        if response.json() is not False:
                            entityId = str(response.json()["id"])
                            # print (response.json())
                        else:
                            entityId = ""
                        print("entityId", entityId)
                    else:
                        bansosReceiverMsisdn = ""

                result.append(
                    {
                        "id": entityId,
                        "name": bansosReceiverNamaKrt,
                        "idNumber": bansosReceiverNikKrt,
                        "msisdn": bansosReceiverMsisdn,
                        "type": "BANSOS_RECEIVER",
                        "bansosReceiverId": bansosReceiverId,
                        "imagePath": s3url,
                        "faceId": faceId,
                        "similarity": similarity
                    }
                )
            else:
                logging.info("faceId not found")
                abort(404, message="faceId not found")
                # logging.info("faceId not found")
                # return {
                #     "message": "faceId not found"
                # }

        return result
