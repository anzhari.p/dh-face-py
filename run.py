from flask import Flask, jsonify, make_response
from flask_restful import Api
from flask_cors import CORS
import resources
import config
import json

app = Flask(__name__)
CORS(app)
api = Api(app)

app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['SECRET_KEY'] = config.SECRET_KEY
# Limit content from length e.g. POST to 2 MB
app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024

api.add_resource(resources.IndexFace, '/indexFace')
api.add_resource(resources.SearchFace, '/searchFace')