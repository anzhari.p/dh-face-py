# dh-face-py
Python implementation of dh-face service.
Written in Python 3.

# Database Schema
Tested on PostgreSQL 11. 
There are two main database schema: 
1. dh-face-py
2. bansos

Create the respective database schema using the template on the db/ directory. 

# Installing
Tested on Ubuntu 18.04 LTS.
Make sure these are installed on the os:
1. python3
2. python3-pip
3. libpq-dev
4. postgresql-client

Clone the repo:
```
git clone https://gitlab.com/anzhari.p/dh-face-py.git
```
  

Get inside the repo. 
Install python dependencies:
```
python3 -m pip install -r requirements.txt
```
Satisfy any required additional dependencies.
Configure the config.py:

```

# SQLALCHEMY ORM DB STRING

DBSTRING_BANSOS_RECEIVER = "postgresql://postgres:Ipnet1!123@localhost:32768/bansos"

DBSTRING_DH_FACE_PY = "postgresql://postgres:Ipnet1!123@localhost:32768/dh-face-py"

  

# AWS Config

AWS_BUCKET = "duithape"

AWS_BUCKET_URL = "https://duithape.s3-ap-southeast-1.amazonaws.com/"

AWS_REKOGNITION_COLLECTION_ID = "bansos-receiver"

  

# Flask secret key for session signing

SECRET_KEY = "you-can-put-uuid-string-here"

```

Prepare AWS SDK Configuration. For ~/.aws/config: 
```
[default]
region = ap-southeast-1
```
And ~/.aws/credentials
```
[default]
aws_access_key_id = access_key_id
aws_secret_access_key = secret_acces_key
```

# Running

  

Test run using:
```
/usr/local/bin/gunicorn --bind 0.0.0.0:5000 wsgi:app
```

The app will start on port 5000 all interface. 

Persist the service by creating your own systemd unit files. 

# To-do
1. Error-handling
2. BANSOS_TRUSTEE
3. Improve SQLAlchemy query speed