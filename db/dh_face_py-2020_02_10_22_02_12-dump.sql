--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.18
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "dh-face-py";
--
-- Name: dh-face-py; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "dh-face-py" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE "dh-face-py" OWNER TO postgres;

\connect -reuse-previous=on "dbname='dh-face-py'"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: m_face; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_face (
    id character varying NOT NULL,
    bansos_receiver_id character varying,
    s3_url character varying,
    aws_face_id character varying
);


ALTER TABLE public.m_face OWNER TO postgres;

--
-- Data for Name: m_face; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.m_face VALUES ('74fcd562-eaae-4577-92b1-55db437d552f', '040fcb17-a99a-45e5-a7f8-5337513cdd82', 'https://duithape.s3-ap-southeast-1.amazonaws.com/0120e312-86fa-4fff-9ee7-3a695c180283', 'bc98c48f-de36-44c9-9c90-76eccf68c85e');
INSERT INTO public.m_face VALUES ('abde0d5a-4914-4e8b-a705-653e89ce30a5', 'a0084b38-5402-42fa-a6ec-563eebd1f754', 'https://duithape.s3-ap-southeast-1.amazonaws.com/ec63e042-65d6-4dd7-b525-9d1395fcd7da', '77b30eae-e2b6-494f-b8a8-85c5178217ad');
INSERT INTO public.m_face VALUES ('a36f24f2-0fe6-4282-8e6f-759b442d1ebe', 'fda4cbba-9a50-4391-a5ca-8d75973f6597', 'https://duithape.s3-ap-southeast-1.amazonaws.com/87a26d9e-f0da-4744-b0c6-d28292429afb', '61422f8b-3af2-4e92-ad9d-3d992bc2430a');
INSERT INTO public.m_face VALUES ('9ccf5a8e-176b-4038-9847-380dc56cab9f', '5ee643be-5558-447c-8da2-565dcb4c8d11', 'https://duithape.s3-ap-southeast-1.amazonaws.com/aec513f5-008e-4e28-9ea2-08b3516288c3', '626d6fdd-8ee9-460f-9014-e3ec8b7f6e83');
INSERT INTO public.m_face VALUES ('4135e152-e264-4821-b693-0ff5d50a49a7', '20237d72-10e7-42e5-86cf-9f137cb3c80a', 'https://duithape.s3-ap-southeast-1.amazonaws.com/ea7fd1fb-cc26-40ee-91dc-b9fcf760328c', '61a2f8aa-f09b-4d07-9e48-6d4491c0e84e');


--
-- Name: m_face m_face_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_face
    ADD CONSTRAINT m_face_pk PRIMARY KEY (id);


--
-- Name: m_face_aws_face_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX m_face_aws_face_id_uindex ON public.m_face USING btree (aws_face_id);


--
-- Name: m_face_bansos_receiver_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX m_face_bansos_receiver_id_uindex ON public.m_face USING btree (bansos_receiver_id);


--
-- Name: m_face_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX m_face_id_uindex ON public.m_face USING btree (id);


--
-- Name: m_face_s3_url_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX m_face_s3_url_uindex ON public.m_face USING btree (s3_url);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: pg1100069275
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM pg1100069275;
GRANT ALL ON SCHEMA public TO pg1100069275;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

