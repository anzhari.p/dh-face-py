# SQLALCHEMY ORM DB STRING
DBSTRING_BANSOS_RECEIVER = "postgresql://postgres:Ipnet1!123@localhost:32768/bansos"
DBSTRING_DH_FACE_PY = "postgresql://postgres:Ipnet1!123@localhost:32768/dh-face-py"

# AWS Config
AWS_BUCKET = "duithape"
AWS_BUCKET_URL = "https://duithape.s3-ap-southeast-1.amazonaws.com/"
AWS_REKOGNITION_COLLECTION_ID = "bansos-receiver"

# Flask secret key for session signing
SECRET_KEY = "ASWmyzvfxAYaxbIoCYrLV9O7l2TFVn1silaCfeEPRXw"


# Internal API 
# Make sure to use prod and dev API respectively!
INTREGRATOR_BASE_URL = "http://dev.duithape.com:9090/vendorgw/api/entity/search?issuerId=6&issuerCode=006&msisdn="